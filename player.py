import pygame
class Player:
    def __init__(self):
        self.health = 1
        self.max_health = 1
        self.velocity = 50
        self.image = pygame.image.load('images/I0.png')
        self.image = pygame.transform.scale(self.image, (77, 146))
        self.rect = self.image.get_rect()
        self.rect.x = 10
        self.rect.y = 610

    def move_d(self):
        self.rect.x += self.velocity

    def move_g(self):
        self.rect.x -= self.velocity

import pygame
class Floor():
    def __init__(self, posX, posY):
        self.image = pygame.image.load('images/asset/floor.jpg').convert_alpha()
        self.image = pygame.transform.scale(self.image, (131, 65))
        self.rect = self.image.get_rect()
        self.rect.x = posX
        self.rect.y = posY
